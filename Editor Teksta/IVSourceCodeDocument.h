//
//  IVSourceCodeDocument.h
//  Editor Teksta
//
//  Created by Ivan Vučica on 8.3.2013..
//  Copyright (c) 2013. Ivan Vučica (TVZ - Programski alati u programiranju). All rights reserved.
//

#import <Cocoa/Cocoa.h>
#if HAVE_LIBCLANG
#import <clang-c/Index.h>
#endif

#define STATIC_ARGS 0

@interface IVSourceCodeDocument : NSDocument<NSTextViewDelegate>

#if !STATIC_ARGS
@property (assign) char ** args;
@property (assign) size_t argCount;
#endif

@property (retain) NSMutableAttributedString * content;
@property (retain) NSMutableAttributedString * incomingContent;
@property (retain) NSArray * diagnostics;

#if HAVE_LIBCLANG
// code completion stuff
@property (nonatomic, assign) CXIndex codeCompletionIndex;
@property (nonatomic, assign) CXTranslationUnit codeCompletionTranslationUnit;
#endif

@property (assign) IBOutlet NSArrayController *diagnosticsArrayController;

@property (assign) IBOutlet NSTextView *textView;
@end
