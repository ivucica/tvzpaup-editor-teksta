//
//  IVPreferencesWindowController.m
//  Editor Teksta
//
//  Created by Ivan Vučica on 6.5.2013..
//  Copyright (c) 2013. Ivan Vučica (TVZ - Programski alati u programiranju). All rights reserved.
//

#import "IVPreferencesWindowController.h"

@interface IVPreferencesWindowController ()

@end

@implementation IVPreferencesWindowController

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}

@end
