//
//  IVAppDelegate.h
//  Editor Teksta
//
//  Created by Ivan Vučica on 6.5.2013..
//  Copyright (c) 2013. Ivan Vučica (TVZ - Programski alati u programiranju). All rights reserved.
//

#import <Foundation/Foundation.h>

@class IVPreferencesWindowController;

@interface IVAppDelegate : NSObject<NSApplicationDelegate>

@property (nonatomic, retain) IVPreferencesWindowController * preferencesWindowController;

- (IBAction)preferences:(id)sender;

@end
