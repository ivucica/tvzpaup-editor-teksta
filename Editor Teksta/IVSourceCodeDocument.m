//
//  IVSourceCodeDocument.m
//  Editor Teksta
//
//  Created by Ivan Vučica on 8.3.2013..
//  Copyright (c) 2013. Ivan Vučica (TVZ - Programski alati u programiranju). All rights reserved.
//

#import "IVSourceCodeDocument.h"

#if STATIC_ARGS
#define DEFAULT_ARGS const char* args[] = { \
    "-x", "objective-c", \
    "-isysroot", "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.8.sdk", \
    "-I", "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.8.sdk/usr/include", \
    "-I", "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.8.sdk/usr/include/c++/4.2.1/tr1", \
    "-I", "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.8.sdk/usr/include/c++/4.2.1", \
    "-mmacosx-version-min=10.6", \
    "-I", [[self.fileURL path] stringByDeletingLastPathComponent].UTF8String \
    };
#endif

@implementation IVSourceCodeDocument

- (id)init
{
    self = [super init];
    if (self) {
        // Add your subclass-specific initialization here.
        
#if HAVE_LIBCLANG
        self.codeCompletionIndex = clang_createIndex(0, 0);
#endif
    }
    return self;
}

- (NSString *)windowNibName
{
    // Override returning the nib file name of the document
    // If you need to use a subclass of NSWindowController or if your document supports multiple NSWindowControllers, you should remove this method and override -makeWindowControllers instead.
    return @"IVSourceCodeDocument";
}

- (void)windowControllerDidLoadNib:(NSWindowController *)aController
{
    [super windowControllerDidLoadNib:aController];
    // Add any code here that needs to be executed once the windowController has loaded the document's window.
}

- (NSData *)dataOfType:(NSString *)typeName error:(NSError **)outError
{
    // Insert code here to write your document to data of the specified type. If outError != NULL, ensure that you create and set an appropriate error when returning nil.
    // You can also choose to override -fileWrapperOfType:error:, -writeToURL:ofType:error:, or -writeToURL:ofType:forSaveOperation:originalContentsURL:error: instead.
    
    [self recolorize:nil];
    
    return [self.content.string dataUsingEncoding:NSUTF8StringEncoding];
    
    if (outError) {
        *outError = [NSError errorWithDomain:NSOSStatusErrorDomain code:unimpErr userInfo:NULL];
    }
    return nil;
}

- (BOOL)readFromData:(NSData *)data ofType:(NSString *)typeName error:(NSError **)outError
{
    // Insert code here to read your document from the given data of the specified type. If outError != NULL, ensure that you create and set an appropriate error when returning NO.
    // You can also choose to override -readFromFileWrapper:ofType:error: or -readFromURL:ofType:error: instead.
    // If you override either of these, you should also override -isEntireFileLoaded to return NO if the contents are lazily loaded.
    NSString * string = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
    NSFont * font = [NSFont fontWithName:@"Menlo" size:12.0];
    NSDictionary * attributes = [[[NSDictionary alloc] initWithObjectsAndKeys:font, NSFontAttributeName, nil] autorelease];
    
    self.content = [[[NSMutableAttributedString alloc] initWithString:string
                                                           attributes:attributes] autorelease];
    
#if HAVE_LIBCLANG

#if 0
    DEFAULT_ARGS;
    size_t argCount = sizeof(args)/sizeof(args[0]);
#else
    NSString * compileOptionsString = compileOptionsString = [[NSUserDefaults standardUserDefaults] stringForKey:@"compileOptionsC"];
    
    if([typeName isEqualToString:(NSString*)kUTTypeCSource])
        compileOptionsString = [[NSUserDefaults standardUserDefaults] stringForKey:@"compileOptionsC"];
    if([typeName isEqualToString:(NSString*)kUTTypeObjectiveCSource])
        compileOptionsString = [[NSUserDefaults standardUserDefaults] stringForKey:@"compileOptionsObjectiveC"];
    if([typeName isEqualToString:(NSString*)kUTTypeCPlusPlusSource])
        compileOptionsString = [[NSUserDefaults standardUserDefaults] stringForKey:@"compileOptionsCPlusPlus"];
    if([typeName isEqualToString:(NSString*)kUTTypeObjectiveCPlusPlusSource])
        compileOptionsString = [[NSUserDefaults standardUserDefaults] stringForKey:@"compileOptionsObjectiveCPlusPlus"];
    NSArray * compileOptionsArray = [compileOptionsString componentsSeparatedByString:@" "];
    if(self.args)
    {
        for(int i = 0; i < self.argCount; i++)
        {
            free(self.args[i]);
        }
        free(self.args);
    }
    
    self.args = malloc(sizeof(const char *) * compileOptionsArray.count);
    for(int i = 0; i < compileOptionsArray.count; i++)
    {
        NSString * s = [compileOptionsArray objectAtIndex:i];
        self.args[i] = malloc(strlen(s.UTF8String)+1);
        strcpy(self.args[i], s.UTF8String);
    }
    self.argCount = compileOptionsArray.count;
    const char ** args = (const char**)self.args; size_t argCount = self.argCount;
#endif
    
    self.codeCompletionTranslationUnit =
        clang_parseTranslationUnit(self.codeCompletionIndex,
                                   [[self.fileURL path] UTF8String],
                                   args, argCount,
                                   NULL, 0,
                                   clang_defaultEditingTranslationUnitOptions());
    [self refreshDiagnostics];
    [self recolorize:nil];
    if(!self.codeCompletionTranslationUnit)
        return NO;
#endif
    return YES;
}
- (void)dealloc
{
    clang_disposeIndex(self.codeCompletionIndex);
    clang_disposeTranslationUnit(self.codeCompletionTranslationUnit);
    
    [super dealloc];
}
+ (BOOL)autosavesInPlace
{
    return YES;
}

- (NSMutableAttributedString*)newAttributedStringFromString:(NSString*)string
{
    NSFont * font = [NSFont fontWithName:@"Menlo" size:12.0];
    NSDictionary * attributes = [[[NSDictionary alloc] initWithObjectsAndKeys:font, NSFontAttributeName, nil] autorelease];
    return [[[NSMutableAttributedString alloc] initWithString:string
                                                   attributes:attributes] autorelease];
}

#if HAVE_LIBCLANG
-(int)reparseTranslationUnit
{
    struct CXUnsavedFile unsavedFile;
    unsavedFile.Contents = [[self.content string] UTF8String];
    unsavedFile.Filename = [self.fileURL.path UTF8String];
    unsavedFile.Length = [[self.content string] length];
    int returnValue = clang_reparseTranslationUnit(self.codeCompletionTranslationUnit, 1, &unsavedFile, clang_defaultReparseOptions(self.codeCompletionTranslationUnit));
    [self refreshDiagnostics];
    return returnValue;
}
-(void)refreshDiagnostics
{
    [self printDiagnostics];
    if(!self.codeCompletionTranslationUnit)
    {
        NSLog(@"No file parsed, cannot diagnose");
        return;
    }
    
    
    int num = clang_getNumDiagnostics(self.codeCompletionTranslationUnit);
    NSMutableArray * diagnostics = [NSMutableArray arrayWithCapacity:num];
    
    for(int i = 0; i < num; i++)
    {
        CXDiagnostic d = clang_getDiagnostic(self.codeCompletionTranslationUnit, i);
        CXString diagnosticDescriptionCLSTR = clang_formatDiagnostic(d,
                                                                     clang_defaultDiagnosticDisplayOptions());
        NSString * diagnosticDescription = [NSString stringWithUTF8String:clang_getCString(diagnosticDescriptionCLSTR) ?: "<none>"];
        //
        CXString diagnosticSpellingCLSTR = clang_getDiagnosticSpelling(d);
        NSString * diagnosticSpelling = [NSString stringWithUTF8String:clang_getCString(diagnosticSpellingCLSTR) ?: "<none>"];
        //
        CXString diagnosticCategoryNameCLSTR = clang_getDiagnosticCategoryName(clang_getDiagnosticCategory(d));
        NSString * diagnosticCategoryName = [NSString stringWithUTF8String:clang_getCString(diagnosticCategoryNameCLSTR) ?: "<none>"];
        //
        NSString * diagnosticSeverity;
        switch(clang_getDiagnosticSeverity(d))
        {
            case CXDiagnostic_Note: diagnosticSeverity = @"Note"; break;
            case CXDiagnostic_Error: diagnosticSeverity = @"Error"; break;
            case CXDiagnostic_Fatal: diagnosticSeverity = @"Fatal"; break;
            case CXDiagnostic_Warning: diagnosticSeverity = @"Warning"; break;
            case CXDiagnostic_Ignored: diagnosticSeverity = @"Ignored"; break;
        }
        
        CXSourceLocation diagnosticLocation = clang_getDiagnosticLocation(d);
        unsigned int diagnosticLocationOffset;
        unsigned int diagnosticLocationLine, diagnosticLocationColumn;
        CXFile diagnosticFile;
        clang_getSpellingLocation(diagnosticLocation, &diagnosticFile, &diagnosticLocationLine, &diagnosticLocationColumn, &diagnosticLocationOffset);
        CXString diagnosticFileNameCLSTR = clang_getFileName(diagnosticFile);
        NSString * diagnosticLocationDescription = [NSString stringWithFormat:@"[%d:%d] %@", diagnosticLocationLine, diagnosticLocationColumn, [[NSString stringWithUTF8String:clang_getCString(diagnosticFileNameCLSTR) ?: "none"] lastPathComponent]];
        
        NSDictionary * diagnostic = [NSDictionary dictionaryWithObjectsAndKeys:
                                     diagnosticCategoryName, @"diagnosticCategoryName",
                                     diagnosticDescription, @"diagnosticDescription",
                                     diagnosticLocationDescription, @"diagnosticLocationDescription",
                                     diagnosticSeverity, @"diagnosticSeverity",
                                     diagnosticSpelling, @"diagnosticSpelling",
                                     nil];
        
        [diagnostics addObject:diagnostic];
        
        clang_disposeString(diagnosticFileNameCLSTR);
        clang_disposeString(diagnosticCategoryNameCLSTR);
        clang_disposeString(diagnosticSpellingCLSTR);
        clang_disposeString(diagnosticDescriptionCLSTR);
        clang_disposeDiagnostic(d);

    }
    self.diagnostics = diagnostics;

}
-(void)printDiagnostics
{
    if(!self.codeCompletionTranslationUnit)
    {
        NSLog(@"No file parsed ok, cannot diagnose");
        return;
    }
    int num = clang_getNumDiagnostics(self.codeCompletionTranslationUnit);
    for(int i = 0; i < num; i++)
    {
        CXDiagnostic d = clang_getDiagnostic(self.codeCompletionTranslationUnit, i);
        CXString s = clang_formatDiagnostic(d,
                                            clang_defaultDiagnosticDisplayOptions());
        fprintf(stderr, "%s\n", clang_getCString(s));
        clang_disposeString(s);
        clang_disposeDiagnostic(d);
    }
}
#endif

#pragma mark - UI actions
enum CXChildVisitResult childVisitor(CXCursor cursor,
                                     CXCursor parent,
                                     CXClientData client_data)
{
    IVSourceCodeDocument * self = (IVSourceCodeDocument*)client_data;
    enum CXCursorKind cursorKind = clang_getCursorKind(cursor);
    CXFile file = clang_getFile(self.codeCompletionTranslationUnit, [self.fileURL.path UTF8String]);
    //clang_getCursorLocation(cursor))
    
    
    CXSourceRange range = clang_getCursorExtent(cursor);
    
    CXSourceLocation start = clang_getRangeStart(range);
    CXSourceLocation end = clang_getRangeEnd(range);
    
    unsigned int startOffset;
    unsigned int endOffset;
    CXFile startFile;
    CXFile endFile;
    clang_getSpellingLocation(start, &startFile, NULL, NULL, &startOffset);
    clang_getSpellingLocation(end, &endFile, NULL, NULL, &endOffset);
    if(startFile != file || endFile != file)
        return CXChildVisit_Continue;
    
    NSRange nsrange = NSMakeRange(startOffset, endOffset-startOffset);

    NSString * s = nil;
    @try { s = [self.incomingContent.string substringWithRange:nsrange]; }
    @catch (NSException *e) { }
    NSLog(@"%@ [%s] - %d:%d", s, clang_getCString(clang_getCursorKindSpelling(cursorKind)), startOffset, endOffset);
    
    NSColor * c = nil;
    enum CXChildVisitResult result = CXChildVisit_Continue;
    switch(cursorKind)
    {
        case CXCursor_CXXMethod:
            result = CXChildVisit_Recurse;
            break;
        case CXCursor_FieldDecl:
            result = CXChildVisit_Recurse;
            break;
        case CXCursor_TypedefDecl:
            result = CXChildVisit_Recurse;
            break;
        case CXCursor_ParmDecl:
            result = CXChildVisit_Recurse;
            break;
        case CXCursor_TypeRef:
            c = [NSColor orangeColor];
            break;
        case CXCursor_Constructor:
            result = CXChildVisit_Recurse;
            break;
        case CXCursor_FunctionDecl:
            result = CXChildVisit_Recurse;
            break;
        case CXCursor_DeclRefExpr:
            c = [NSColor blueColor];
            break;
        case CXCursor_PreprocessingDirective:
            c = [NSColor brownColor];
            break;
        case CXCursor_BlockExpr:
            result = CXChildVisit_Recurse;
            break;
        case CXCursor_StructDecl:
            result = CXChildVisit_Recurse;
            break;
            /*
        case 202: // compoundstmt
            result = CXChildVisit_Recurse;
            break;
        case 109: // string literal
            c = [NSColor redColor];
            break;
             */
        case CXCursor_MemberRef:
            c = [NSColor blueColor];
            break;
        case CXCursor_MemberRefExpr:
            c = [NSColor blueColor];
            break;
        case CXCursor_VarDecl:
            result = CXChildVisit_Recurse;
            break;
        case CXCursor_Destructor:
            result = CXChildVisit_Recurse;
            break;
        case CXCursor_UnexposedExpr:
            result = CXChildVisit_Continue;
            break;
        case CXCursor_ObjCClassRef:
            c = [NSColor redColor];
            break;
        default:
            NSLog(@"!!!!!! %d", cursorKind);
            
    }

    
    //NSDictionary * colorAttribute = [NSDictionary dictionaryWithObjectsAndKeys:c, NSForegroundColorAttributeName, nil];
    
    if(c)
    {
        @try {
            [self.incomingContent addAttribute:NSForegroundColorAttributeName value:c range:nsrange];
        }
        @catch (NSException *exception) {
            NSLog(@"...failed to set color : %@", exception);
        }
        @finally {
        }
    }
    
    return CXChildVisit_Recurse; // TODO: return result
}
-(IBAction)recolorize:(id)sender
{
    [self recolorizeTokenBased];

    clang_visitChildren(clang_getTranslationUnitCursor(self.codeCompletionTranslationUnit), childVisitor, self);
    
    self.content = self.incomingContent;
}
-(void)recolorizeTokenBased
{
    [self reparseTranslationUnit];
    
    NSMutableAttributedString * mas = [self newAttributedStringFromString:self.content.string];
    self.incomingContent = mas;
    
    CXFile file = clang_getFile(self.codeCompletionTranslationUnit, [self.fileURL.path UTF8String]);
    CXSourceLocation startLocation = clang_getLocationForOffset(self.codeCompletionTranslationUnit, file, 0);
    CXSourceLocation endLocation = clang_getLocationForOffset(self.codeCompletionTranslationUnit, file, (unsigned int)self.content.string.length);
    CXSourceRange entireFileRange = clang_getRange(startLocation, endLocation);

    CXToken * tokens;
    unsigned int numTokens;
    clang_tokenize(self.codeCompletionTranslationUnit, entireFileRange, &tokens, &numTokens);
    
    CXCursor cursors[numTokens];
    clang_annotateTokens(self.codeCompletionTranslationUnit, tokens, numTokens, cursors);
    
    for(int i = 0; i < numTokens; i++)
    {
        CXToken token = tokens[i];
        CXSourceLocation location = clang_getTokenLocation(self.codeCompletionTranslationUnit, token);
        //CXCursor cursor = clang_getCursor(self.codeCompletionTranslationUnit, location);
        CXCursor cursor = cursors[i];
        CXTokenKind kind = clang_getTokenKind(token);
        
        NSColor * c = nil;
        switch(kind)
        {
            case CXToken_Keyword:
                c = [NSColor purpleColor];
                break;
            case CXToken_Comment:
                c = [NSColor colorWithCalibratedRed:0 green:0.5 blue:0 alpha:1.];
                break;
            case CXToken_Identifier:
            {
                c = [NSColor blackColor];
#if 0
                const char *start = NULL, *end = NULL;
                unsigned startCol, endCol, startRow, endRow;
                clang_getDefinitionSpellingAndExtent(cursor, &start, &end, &startCol, &endCol, &startRow, &endRow);
                for(const char * p = start; p != end; p++)
                {
                    printf("%c", *p);
                }
                printf("\n");
#endif
                break;
            }
            case CXToken_Literal:
                c = [NSColor redColor];
                break;
            case CXToken_Punctuation:
                c = [NSColor blackColor];
                break;
        }
        
        NSLog(@"[%s]", clang_getCString(clang_getCursorKindSpelling(clang_getCursorKind(cursor))));
        if(clang_getCursorKind(cursor) != CXCursor_InvalidFile)
            childVisitor(cursor, clang_getNullCursor(), self);
            
        CXSourceRange range = clang_getTokenExtent(self.codeCompletionTranslationUnit, token);
     
        CXSourceLocation start = clang_getRangeStart(range);
        CXSourceLocation end = clang_getRangeEnd(range);
        
        NSDictionary * colorAttribute = [NSDictionary dictionaryWithObjectsAndKeys:c, NSForegroundColorAttributeName, nil];
        
        unsigned int startOffset;
        unsigned int endOffset;
        clang_getSpellingLocation(start, file, NULL, NULL, &startOffset);
        clang_getSpellingLocation(end, file, NULL, NULL, &endOffset);
        NSRange nsrange = NSMakeRange(startOffset, endOffset-startOffset);
        if(c)
            [mas addAttribute:NSForegroundColorAttributeName value:c range:nsrange];
    }
    
    self.incomingContent = mas;
}

#pragma mark - Autocomplete
#if HAVE_LIBCLANG
-(NSArray*)textView:(NSTextView *)textView completions:(NSArray *)words forPartialWordRange:(NSRange)charRange indexOfSelectedItem:(NSInteger *)index
{
    if(!self.codeCompletionTranslationUnit)
    {
        return words;
    }
    [self reparseTranslationUnit];
    
    CXFile file = clang_getFile(self.codeCompletionTranslationUnit, [self.fileURL.path UTF8String]);
    CXSourceLocation location = clang_getLocationForOffset(self.codeCompletionTranslationUnit, file, charRange.location + charRange.length);
    
    unsigned int line;
    unsigned int column;
    unsigned int offset;
    
    clang_getSpellingLocation(location, &file, &line, &column, &offset);
    
    struct CXUnsavedFile unsavedFile;
    unsavedFile.Contents = [[self.content string] UTF8String];
    unsavedFile.Filename = [self.fileURL.path UTF8String];
    unsavedFile.Length = [[self.content string] length];
    NSLog(@"Working on %d %d", line, column);
    CXCodeCompleteResults *results = clang_codeCompleteAt(self.codeCompletionTranslationUnit,
                                                          [self.fileURL.path UTF8String],
                                                          line, column,
                                                          &unsavedFile, 1,
                                                          0);
    
    clang_sortCodeCompletionResults(results->Results, results->NumResults);
    
    NSMutableArray * ma = [NSMutableArray arrayWithCapacity:results->NumResults];
    NSInteger bestIndex = 0;
    NSInteger bestPriority = 9000;
    
    NSString * replacedText = [textView.string substringWithRange:charRange];
    
    for(int i = 0; i < results->NumResults; i++)
    {
        CXCompletionResult result = results->Results[i];
        CXCompletionString completion = result.CompletionString;
        
        NSMutableString * ms = [NSMutableString string];
        for(int j = 0; j < clang_getNumCompletionChunks(completion); j++)
        {
            if(clang_getCompletionChunkKind(completion, j) != CXCompletionChunk_TypedText)
                continue;
            CXString string = clang_getCompletionChunkText(completion, j);
            [ms appendString:[NSString stringWithUTF8String:clang_getCString(string)]];
            clang_disposeString(string);
        }
        
        if(result.CursorKind == CXCursor_FieldDecl)
        {
            [ms insertString:replacedText atIndex:0];
        }
        
        unsigned priority = clang_getCompletionPriority(completion);
        //NSLog(@"Priority %d for %@", priority, ms);
        NSDictionary * priorityAndString = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:priority], @"priority", ms, @"string", nil];
        [ma addObject:priorityAndString];
    }
    
    if(!ma.count)
    {
        
        clang_disposeCodeCompleteResults(results);
        
        *index = bestIndex;
        return [NSArray array];
        
    }
    
    /*
    {
        NSMutableArray * ma2 = [ma mutableCopy];
        [ma2 sortUsingDescriptors:[NSArray arrayWithObject:
                                   [NSSortDescriptor sortDescriptorWithKey:@"priority" ascending:YES]]];
        
        bestPriority = [[[ma2 objectAtIndex:0] valueForKey:@"priority"] intValue];
    }
     */
    
    NSMutableArray * ourWords = [NSMutableArray arrayWithCapacity:ma.count];
    
    for(int i = 0 ; i < ma.count; i++)
    {
        NSInteger priority = [[[ma objectAtIndex:i] valueForKey:@"priority"] integerValue];
        NSString * word = [[ma objectAtIndex:i] valueForKey:@"string"];
        
        if(((word.length >= charRange.length && [[word substringToIndex:charRange.length] isEqualToString:[self.content.string substringWithRange:charRange]]) || charRange.length == 0))
        {
            /*
            if(ourWords.count && word.length <= [[ourWords objectAtIndex:bestIndex] length])
                bestIndex = ourWords.count;
             */
            if(priority < bestPriority)
            {
                bestPriority = priority;
                bestIndex = ourWords.count;
            }
            [ourWords addObject:word];
        }
    }
    
    if(ourWords.count == 0)
    {
        for(int i = 0 ; i < ma.count; i++)
        {
            int priority = [[[ma objectAtIndex:i] valueForKey:@"priority"] intValue];
            NSString * word = [[ma objectAtIndex:i] valueForKey:@"string"];
            
            if(priority >= bestPriority)
            {
                [ourWords addObject:word];
            }
        }
        bestIndex = 0;
    }
    
    clang_disposeCodeCompleteResults(results);
    
    *index = bestIndex;
    return ourWords;
}
#endif

- (BOOL)textView:(NSTextView *)textView shouldChangeTextInRange:(NSRange)affectedCharRange replacementString:(NSString *)replacementString
{
    if(replacementString.length)
    {
        if([[replacementString substringFromIndex:replacementString.length - 1] isEqualToString:@"."])
        {
            // perform [textView complete:textView] on next
            // runloop step
            
            [textView performSelector:@selector(complete:) withObject:textView afterDelay:0.0];
        }
        if([[replacementString substringFromIndex:replacementString.length - 1] isEqualToString:@" "])
        {
            [textView performSelector:@selector(complete:) withObject:textView afterDelay:0.0];
        }
    }
    return YES;
}
@end
