//
//  IVDocument.m
//  Editor Teksta
//
//  Created by Ivan Vučica on 8.3.2013..
//  Copyright (c) 2013. Ivan Vučica (TVZ - Programski alati u programiranju). All rights reserved.
//

#import "IVDocument.h"

@implementation IVDocument

- (id)init
{
    self = [super init];
    if (self) {
        // Add your subclass-specific initialization here.
    }
    return self;
}

- (NSString *)windowNibName
{
    // Override returning the nib file name of the document
    // If you need to use a subclass of NSWindowController or if your document supports multiple NSWindowControllers, you should remove this method and override -makeWindowControllers instead.
    return @"IVDocument";
}

- (void)windowControllerDidLoadNib:(NSWindowController *)aController
{
    [super windowControllerDidLoadNib:aController];
    // Add any code here that needs to be executed once the windowController has loaded the document's window.
}

+ (BOOL)autosavesInPlace
{
    return YES;
}

- (NSData *)dataOfType:(NSString *)typeName error:(NSError **)outError
{
    if([typeName isEqualToString:@"public.plain-text"])
    {
        // plaintext
        return [self.content.string dataUsingEncoding:NSUTF8StringEncoding];
    }
    else
    {
        // rtf
        return [self.content RTFFromRange:NSMakeRange(0, self.content.string.length) documentAttributes:nil];
    }
}

- (BOOL)readFromData:(NSData *)data ofType:(NSString *)typeName error:(NSError **)outError
{
    if([typeName isEqualToString:@"public.plain-text"])
    {
        // plaintext
        NSString * string = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
        NSFont * font = [NSFont fontWithName:@"Courier" size:12.0];
        NSDictionary * attributes = [[[NSDictionary alloc] initWithObjectsAndKeys:font, NSFontAttributeName, nil] autorelease];
        self.content = [[[NSAttributedString alloc] initWithString:string
                                                        attributes:attributes] autorelease];
    }
    else
    {
        // rtf
        self.content = [[[NSAttributedString alloc] initWithRTF:data documentAttributes:nil] autorelease];
    }
    return YES;
}

@end
