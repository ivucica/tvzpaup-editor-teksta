//
//  IVDocument.h
//  Editor Teksta
//
//  Created by Ivan Vučica on 8.3.2013..
//  Copyright (c) 2013. Ivan Vučica (TVZ - Programski alati u programiranju). All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface IVDocument : NSDocument

@property (retain) NSAttributedString * content;

@property (assign) IBOutlet NSTextView *contentTextView;

@end
