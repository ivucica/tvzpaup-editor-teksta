//
//  IVPreferencesWindowController.h
//  Editor Teksta
//
//  Created by Ivan Vučica on 6.5.2013..
//  Copyright (c) 2013. Ivan Vučica (TVZ - Programski alati u programiranju). All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface IVPreferencesWindowController : NSWindowController

@end
