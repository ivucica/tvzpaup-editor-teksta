//
//  IVAppDelegate.m
//  Editor Teksta
//
//  Created by Ivan Vučica on 6.5.2013..
//  Copyright (c) 2013. Ivan Vučica (TVZ - Programski alati u programiranju). All rights reserved.
//

#import "IVAppDelegate.h"
#import "IVPreferencesWindowController.h"

@implementation IVAppDelegate

-(void)applicationWillFinishLaunching:(NSNotification *)notification
{
    NSDictionary * dict;
    dict = [NSDictionary dictionaryWithObjectsAndKeys:
            @"-x c "
            "-isysroot /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.8.sdk "
            "-mmacosx-version-min=10.6 "
            ,
            @"compileOptionsC",
            
            
            @"-x objective-c "
            "-isysroot /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.8.sdk "
            "-mmacosx-version-min=10.6 "
            ,
            @"compileOptionsObjectiveC",
            
            
            @"-x c++ "
            "-isysroot /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.8.sdk "
            "-I /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.8.sdk/usr/include "
            "-I /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.8.sdk/usr/include/c++/4.2.1/tr1 "
            "-I /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.8.sdk/usr/include/c++/4.2.1 "
            "-mmacosx-version-min=10.6 "
            ,
            @"compileOptionsCPlusPlus",
            
            
            @"-x objective-c++ "
            "-isysroot /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.8.sdk "
            "-I /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.8.sdk/usr/include "
            "-I /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.8.sdk/usr/include/c++/4.2.1/tr1 "
            "-I /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.8.sdk/usr/include/c++/4.2.1 "
            "-mmacosx-version-min=10.6 "
            ,
            @"compileOptionsObjectiveCPlusPlus",
            
            
            nil];
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:dict];
}

-(void)preferences:(id)sender
{
    if(!self.preferencesWindowController)
    {
        self.preferencesWindowController = [[[IVPreferencesWindowController alloc] initWithWindowNibName:@"IVPreferencesWindowController"] autorelease];
    }
    
    [self.preferencesWindowController showWindow:sender];
}
@end
